function _step_durations_from_notes(notes){
    return notes.map(note => {
        if(note.type == "r")
            return new Fraction(-1, note.value);
        else if(note.type == "n")
            return new Fraction(1, note.value);
        else // bar
            return new Fraction(0);
    });
}

function _step_dots(notes, values){
    return values.map((v, i) => {
        if(notes[i].dot)
            return v.mul(1.5);
        return v;
    });
}

function _getThisTupletType(notes, idx){
    for(let i = idx; i < notes.length; i++){
        let note = notes[i];
        if(note.tuplet_end)
            return note.tuplet_type;
    }
    throw "[rhythmUtilities] tuplet_end missing.";
}

function _step_tuplets(notes, values) {

    let current_tuplet_type = null;
    return values.map((v,i) => {
        let nV = v;
        if(notes[i].in_tuplet){
            if(current_tuplet_type == null){
                current_tuplet_type = _getThisTupletType(notes, i);
            }
            nV = nV.div(current_tuplet_type.num_notes).mul(current_tuplet_type.in_space_of);
        }
        else {
            current_tuplet_type = null;
        }
        if(notes[i].tuplet_end){
            current_tuplet_type = null;
        }
        return nV;
    });
}

function _step_ties(notes, values) {
    let newValues = [];
    let currentDuration = new Fraction(0), 
        previousType = "bar";
    for(let i = 0; i < notes.length; i++){
        const note = notes[i], 
             value = values[i];
        if(note.type == "bar") continue;
        if(previousType == "bar"){
            previousType = note.type;
            currentDuration = currentDuration.add(value);
            continue;
        }
        if(note.type != previousType || !note.tie){
            newValues.push(currentDuration);
            currentDuration = new Fraction(0);
        }
        currentDuration = currentDuration.add(value);
        previousType = note.type;
    }

    if(currentDuration.abs() != new Fraction(0)){
        newValues.push(currentDuration);
    }
    return newValues;

}

function _step_pauses(values) {

    let newValues = [];
    let currentDuration = null;
    for(let i = 0; i < values.length; i++){
        const value = values[i];
        if(value.valueOf() < 0) {
            if(currentDuration != null){
                currentDuration = currentDuration.add(value);
            }else {
                currentDuration = value;
            }
        } else {
            if(currentDuration != null){
                newValues.push(currentDuration);
                currentDuration = null;
            }
            newValues.push(value);
        }
    }
    if(currentDuration != null){
        newValues.push(currentDuration);
    }
    return newValues;
}

function _generate_playback_durations(notes){
    let v = [];
    // Pretvori note tipa musison v ulomke
    // Če gre za pavzo, naj ima ulomek negativen predznak
    // Vrne polje, ki je enako dolgo kot zapis musison
    v = _step_durations_from_notes(notes);
    // Preveri, če imajo note pike
    // Ulomek, ki predstavlja noto s piko, pomnoži s 3/2
    v = _step_dots(notes, v);
    // Preveri, če so note vezane v poddelitve
    // Ulomke vezanih not pomnoži s koeficientom poddelitve
    v = _step_tuplets(notes, v);
    // Preveri, če so note vezane z vezaji
    // Ulomke, katerih note so vezane z vezaji, seštej skupaj
    // tako da iz dveh nastane eden
    v = _step_ties(notes, v); 
    // Na tej stopnji ulomki niso več nujno usklajeni z zapisom musison
    // Preveri, če obstajajo zaporedne pavze
    // Več zaporednih ulomkov z negativnim predznakom seštej in naredi enega
    v = _step_pauses(v); 
    return v;
}