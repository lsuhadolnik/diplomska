private function weightedRandomSelector($options, $selector)
{
    $rand = rand(0, 1000) / 1000;
    $sum = array_sum(array_map($selector, $options));
    $cumsum = 0;
    $vals = array_map(function($el) use(&$sum, &$selector, &$cumsum) { 
        $cumsum += $selector($el) / $sum;
        return $cumsum; 
    }, $options);
    for($i = 0; $i < count($vals); $i++){
        if ($rand <= $vals[$i]) {
            return $options[$i];
        }
    } 
    return null;
}