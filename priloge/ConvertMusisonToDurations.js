function _generate_playback_durations(notes) {
    let v = [];
    // Pretvori note v notranji predstavitvi v ulomke
    // Ce gre za pavzo, naj ima ulomek negativen predznak
    // Vrne polje, ki je enako dolgo kot notranja predstavitev
    v = _step_durations_from_notes(notes);
    // Preveri, ce imajo note pike
    // Ulomek, ki predstavlja noto s piko, pomnozi s 3/2
    v = _step_dots(notes, v);
    // Preveri, ce so note vezane v poddelitve
    // Ulomke vezanih not pomnozi s koeficientom poddelitve
    v = _step_tuplets(notes, v);
    // Preveri, ce so note vezane z vezaji
    // Ulomke, katerih note so vezane z vezaji, sestej skupaj
    // tako da iz dveh nastane eden
    v = _step_ties(notes, v); 
    // Na tej stopnji ulomki niso vec nujno usklajeni z notranjo predstavitvijo
    // Preveri, ce obstajajo zaporedne pavze
    // Vec zaporednih ulomkov z negativnim predznakom sestej in naredi enega
    v = _step_pauses(v); 
    return v;
}  